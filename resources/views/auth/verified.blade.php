@extends('layouts.app')
@section('titleApp')
@lang('general.verifiedAccount')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        @if($is)
                        <div class="col-md-8">@lang('general.accountConfirmed')</div>
                        <div class="col-md-4"><a href="{{ url('/home') }}">@lang('general.goToHome')</a></div>
                        @else
                        <div class="col-md-8">@lang('general.failedToConfirmAccount')</div>
                        <div class="col-md-4"></div>
                        @endif
                    </div>
                </div>
                <!--<div class="panel-body"></div>-->
            </div>
        </div>
    </div>
</div>
@endsection
