<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&libraries=places" type="text/javascript"></script>-->
            <style type="text/css">
            #mapDetail {
                height:  300px;
            }
            </style>
            @if($isModal)
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">{{ ($report->title != '' ? $report->title : 'Sin título') }}</h4>
                </div>
                <div class="modal-body">
                @endif

                Tipo:
                <p>{{ $report->type_id }}</p>
                Description:
                <p>{{ $report->description }}</p>
                <br>
                Dirección:
                <p>{{ $report->address }}</p>
                <b>Fecha: {{ $report->eventDate }}</b>
                <div id="mapDetail"></div>

                @if($isModal)
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

                <input type="hidden" id="lng-d" value="{{ $report->longitude }}">
                <input type="hidden" id="lat-d" value="{{ $report->latitude }}">
            </div>
            </div>
            </form>
            @endif
            <script type="text/javascript">
            $(document).ready(function() {
                console.log('ready modal detail');
                var lat = {{ $report->latitude }};
                var lng = {{ $report->longitude }};
                initMapDetail(lat,lng);
            });
            </script>