<?php
/**
 * @author kwn
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
//use App\Http\Controllers\Controller;
use App\Report;
use App\Http\Controllers\Session;

use DB;
use App\Quotation;

class ReportController extends Controller
{

    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware('auth', ['except' => ['searchInMap', 'listReport', 'getDetailMapReport']]);
    }

    public function index()
    {
    	$report = '';
    	$report = Report::find(1);
    	var_dump($report);
    }

    public function create()
    {
    	$user = \App\User::find(\Auth::user()->id);
    	$types = \App\Type::all();
    	return view('report.create')->with('user', $user)->with('types', $types);
    }

    protected function store(Request $request)
    {
    	$report = new Report($request->all());
    	$report->type_id = $request->type;
    	$report->user_id = \Auth::user()->id;
    	$numberDocument = isset($request->numberDocument) ? $request->numberDocument : 0;
    	$report->address = $request->adds;
    	$report->latitude = $request->lat;
    	$report->longitude = $request->long;
    	$report->eventDate = date("Y-m-d H:i:s", strtotime($request->eventDate));
    	$report->status = 0;
    	$report->save();

    	if($numberDocument != 0) {
    		$user = \App\User::find(\Auth::user()->id);
    		$user->numberDocument = $numberDocument;
    		$user->save();
    	}

        $user = \App\User::find(\Auth::user()->id);
        flash('Denuncia registrada.', 'success');
        return view('home')->with('user', $user);
    }

    protected function searchInMap(Request $request)
    {
        header("Content-type: text/xml");
        $results = $this->querySearchInMap($request);

        $return = '';

        if(count($results) > 0) {
            $return .= "<markers>\n";
            foreach ($results as $key => $result) {
              $return .= '<marker ';
              $return .= 'title="' . $this->parseToXML($result->title) . '" ';
              $return .= 'address="' . $this->parseToXML($result->address) . '" ';
              $return .= 'lat="' . $result->latitude . '" ';
              $return .= 'lng="' . $result->longitude . '" ';
              $return .= 'distance="' . $result->distance . '" ';
              $return .= '_id="' . $result->id . '" ';

              /*$return .= 'title="' . $result->title . '" ';
              $return .= 'description="' . $result->description . '" ';*/
              $return .= "/>\n";
            }
            $return .= "</markers>\n";
        }
        echo $return;
    }

    protected function searchInMapDetail()
    {
        $results = $this->querySearchInMap($request);
        //pasarlos a un html
        //debolver el html en un array json para mostrarlo debajo del mapa como detalles
    }

    public function querySearchInMap($request)
    {
       return DB::select('SELECT id, address, title, latitude, longitude, ( 3959 * acos( cos( radians(:lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(:long) ) + sin( radians(:lat_) ) * sin( radians( latitude ) ) ) ) AS distance FROM reports HAVING distance < :radio ORDER BY distance LIMIT 0 , 20', ['lat' => $request->lat, 'long' => $request->long, 'lat_' => $request->lat, 'radio' => $request->radio]);
    }

    protected function parseToXML($htmlStr)
    {
        $xmlStr=str_replace('<','&lt;',$htmlStr);
        $xmlStr=str_replace('>','&gt;',$xmlStr);
        $xmlStr=str_replace('"','&quot;',$xmlStr);
        $xmlStr=str_replace("'",'&#39;',$xmlStr);
        $xmlStr=str_replace("&",'&amp;',$xmlStr);
        return $xmlStr;
    }

    protected function listReport(Request $request)
    {
        return view('report.list')->with('title', $request->title)->with('address', $request->address)->with('dateEvent', $request->dateEvent);
    }

    public function getDetailMapReport(Request $request)
    {
        $report = \App\Report::find($request->_id);
        return view('report.tagReportDetail')->with('report', $report)->with('isModal', true);
    }
}
