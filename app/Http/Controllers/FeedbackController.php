<?php
/**
 * @author kwn
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

use App\Report;
use App\Http\Controllers\Session;

class FeedbackController extends Controller
{
    public function index()
    {
    	$isSession = isset(\Auth::user()->id) ? true : false;
        return view('feedback.index')->with('isModal', false)->with('isSession', $isSession);
    }

    public function getTagIndex()
    {
    	$isSession = isset(\Auth::user()->id) ? true : false;
    	return view('feedback.tagIndex')->with('isModal', true)->with('isSession', $isSession);
    }

    protected function store(Request $request)
    {
    	//var_dump(\Auth::user()); exit();
    	$dataMail = array(
    		'user_id' => isset(\Auth::user()->id) ? \Auth::user()->id : null,
    		'names' => isset(\Auth::user()->id) ? \Auth::user()->name.' '.\Auth::user()->name :  $request->names,
    		'email' => isset(\Auth::user()->id) ? \Auth::user()->email : $request->email,
    		'type' => '('.$request->type.') '.$this->types($request->type),
    		'description' => $request->description
    	);
        Mail::send('feedback.mail', ['dataMail' => $dataMail], function ($message) {
            $message->to('kwning.sc@gmail.com', 'KWN')->subject('Feedback Wander.pe');//modificar por el gmail de wander
        });
        flash('Gracias por tu aporte!', 'success');
        return view('welcome');
    }

    public function types($id)
    {
    	$str = '';
    	if($id == 1) {
    		$str = 'Error';
    	}
    	return $str;
    }
}
