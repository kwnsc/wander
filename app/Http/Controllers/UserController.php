<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Quotation;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function confirmation($token)
    {
      	$user = DB::table('users')->where('tokenEmail', $token)->first();
    	if($user != null) {
    		DB::table('users')->where('tokenEmail', $token)->update(['status' => 1]);
    		return view('auth.verified')->with('is', true);
    	} else {
    		return view('auth.verified')->with('is', false);
    	}
    }

    protected function edit()
    {
        $user = \App\User::find(\Auth::user()->id);
        return view('user.edit')->with('user', $user);
    }

    protected function update(Request $request)
    {
        $user = \App\User::find(\Auth::user()->id);
        $user->name = $request->name;
        $user->lastName = $request->lastName;
        $user->birthdate =$request->birthdate;
        $user->save();

        flash('Usuario editado.', 'success');
        //return view('user.edit')->with('user', $user);
        return view('home')->with('user', $user);
    }
}
