@extends('layouts.app')
@section('titleApp')
@lang('general.registerDenounce')
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}"/>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&sensor=true"></script>
<script type="text/javascript" src="{{ asset('js/map-add-report.js') }}"></script>
@endsection

@section('content')
<div class="container bootstrap-iso">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('general.registerDenounce')</div>
                <div class="panel-body">
                    @include('feedback.tagIndex')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
