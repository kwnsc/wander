/**
 * @author kwn
 */
 $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})
$(document).ready(function(){
	if($('#eventDate').length) {
		$('#eventDate').datetimepicker();
	}
	if($('#birthdate').length) {
		$('#birthdate').datetimepicker({format: 'YYYY-MM-DD'});
	}

	$('.feedback').on( 'click', function( event ) {
		loadModalFeedback();
		event.preventDefault();
	});
	$('.login').on( 'click', function( event ) {
		loadModalLogin();
		event.preventDefault();
	});
	$('.register').on( 'click', function( event ) {
		loadModalRegister();
		event.preventDefault();
	});
	/*$(document).on('click', '.submit-feedback', function(){
		$( "#add-feedback" ).submit();
	});*/

	$(document).on('click', '.map-detail-report', function() {
		loadModalReportDetail($(this));
	});
})

function loadModalLogin() {
	loadSL();
	$.post( '/getTagLogin', function( data ) {
		$('#myModal').html(data);
	});
}

function loadModalRegister() {
	loadSL();
	$.post( '/getTagRegister', function( data ) {
		$('#myModal').html(data);
	});
}

function loadModalFeedback() {
	loadSL();
	$.post( '/feedback/getTagIndex', function( data ) {
		$('#myModal').html(data);
	});
}

function loadModalReportDetail(t) {
	var id = t.attr('data-report');
	//alert('el id es: '+id);
	loadSL();
	$.post( '/report/getDetailMapReport', {_id: id} , function( data ) {
		console.log(data);
		$('#myModal').html(data);
	});
}

function loadSL() {
	$('#myModal').html('');
}