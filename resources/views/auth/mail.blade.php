@lang('general.hi') {{ $dataMail['name'] }}, @lang('general.welcomeConfirmation').<br>
<a href="{{ url('/user/confirmation') }}/{{ $dataMail['token'] }}">@lang('general.confirmAccount')</a>