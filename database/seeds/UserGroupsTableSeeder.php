<?php
/**
 * @author kwn
 */
use Illuminate\Database\Seeder;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('userGroups')->insert([
        	[
        		'isUser' => 1,
        		'name' => 'Team Wander',
        		'description' => '',
        		'phone' => '',
        		'address' => '',
        		'latitude' => 1,
        		'longitude' => 1,
        	],
        	[
        		'isUser' => 1,
        		'name' => 'Users',
        		'description' => '',
        		'phone' => '',
        		'address' => '',
        		'latitude' => 1,
        		'longitude' => 1,
        	],
        	/*[
        		'isUser' => 0,
        		'name' => 'Comisaria A',
        		'description' => '',
        		'phone' = '',
        		'address' => '',
        		'latitude' => 1,
        		'longitude' => 1,
        	],*/
        ]);
    }
}
