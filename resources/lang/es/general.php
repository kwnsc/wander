<?php
/**
 * @author kwn
 */

return array(
	'welcome' => 'Bienvenido a ',
	'home' => 'Inicio',
	'search' => 'Buscar',
	'login' => 'Iniciar Sesión',
	'signup' => 'Registro',
	'editUser' => 'Editar usuario',
	'logout' => 'Cerrar Sesión',

	'registerDenounce' => 'Registrar Denuncia',
	'numberDocument' => 'Número de documento',
	'typeDenounce' => 'Tipo de denuncia',
	'titleDenounce' => 'Título',
	'descriptionDenounce' => 'Descripción',
	'eventDate' => 'Fecha y Hora',
	'address' => 'Dirección',

	'emailAddress' => 'Dirección de e-mail',
	'password' => 'Contraseña',
	'rememberMe' => 'Recuérdame',
	'forgotYourPassword' => '¿Olvidaste tu contraseña?',
	'loginWithFacebook' => 'Ingresar con Facebook',
	'signupWithFacebook' => 'Registrarse con Facebook',

	'name' => 'Nombres',
	'lastName' => 'Apellidos',
	'confirmPassword' => 'Confirmar contraseña',
	'hi' => 'Hola',
	'welcomeConfirmation' => 'bienvenid@ a Wander.pe',
	'confirmAccount' => 'Confirmar cuenta',

	'verifiedAccount' => 'Verificar cuenta',
	'accountConfirmed' => 'Cuenta Confirmada!',
	'goToHome' => 'Ir al inicio',
	'failedToConfirmAccount' => 'Error al confirmar cuenta',

	'cancel' => 'Cancelar',
	'sendAdd' => 'Registrar',
	'sendUpdate' => 'Actualizar',

	'birthdate' => 'Fecha de nacimiento',

	'description' => 'Descripción',
	'names' => 'Nombres',
	'category' => 'Categoría',

	'hasToBeConfirmed' => 'Cuenta por confirmar',
	'findPlace' => 'Buscar Lugar',
);