@extends('layouts.app')
@section('titleApp')
@lang('general.home')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('flash::message')
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                      <div class="col-md-8">@lang('general.welcome') Wander.pe</div>
                      @if($user->status != 0)
                      <div class="col-md-4"><a class="btn btn-danger" href="{{ url('/report/create') }}" role="button">@lang('general.registerDenounce')</a></div>
                      @else
                      <div class="col-md-4"><a">@lang('general.hasToBeConfirmed')</a></div>
                      @endif
                    </div>
                </div>
                <!--<div class="panel-body"></div>-->
            </div>
        </div>
    </div>
</div>
@endsection
