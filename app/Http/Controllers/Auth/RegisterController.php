<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'lastName' => 'required|max:255',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $token = str_random(20);
        $dataMail = array('token' => $token, 'name' => $data['name']);
        Mail::send('auth.mail', ['dataMail' => $dataMail], function ($message) {
            $message->to('kwning.sc@gmail.com', 'KWN')->subject('Confirmación de la cuenta en Wander.pe');
        });

        return User::create([
            'userGroups_id' => 2,
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'numberDocument' => 0,
            'username' => $data['email'],
            'lastName' => $data['lastName'],
            'birthdate' => '1991-09-20',
            'img' => '',
            'role' => 0,
            'status' => 0,
            'tokenEmail' => $token,
        ]);
    }

    public function converterUsername($username)
    {
        $position = explode('@', $username);
        return (isset($position[0]) ? $position[0] : '');
    }
}
