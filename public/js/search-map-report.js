/**
 * @author kwn
 */
$(document).ready(function() {
  console.log('ready');
  load();
  /*$( '#radiusSelect' ).on( 'submit', function( event ) {
    searchReportMap(event);
  });*/
  $(document).keypress(function(event) {
    if(event.which == 13) {
      searchReportMap(event);
    }
  });
  $( '#btn-search-report-map' ).on( 'click', function( event ) {
    searchReportMap(event);
  });
});

function searchReportMap(event) {
  //alert('submit!');
  searchLocations();
}

var map;
var panorama;

    var markers = [];
    var infoWindow;
    var locationSelect;
    var center = new google.maps.LatLng(-5.1782884,-80.65488820000002);
    var centerUser;
    var zoom = 4;//default;
    var zoomLevel;

    function load() {
      console.log('function load');
      //if($('#map').length > 0) { alert('encontrado'); $('#map').html('<h1>Holi</h1>') } else { alert('no encontrado'); }
      map = new google.maps.Map(document.getElementById("map"), {
        center: center,
        zoom: zoom,
        mapTypeId: 'roadmap',
        mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU}
      });
      infoWindow = new google.maps.InfoWindow();

      locationSelect = document.getElementById("locationSelect");
      locationSelect.onchange = function() {
        var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
        if (markerNum != "none"){
          google.maps.event.trigger(markers[markerNum], 'click');
        }
      };

        var input = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('addressInput')),
            {types: ['geocode']});


        map.addListener('zoom_changed', function() {
          zoomLevel = map.getZoom();
          
          console.log('h.... center: '+centerUser+' radio: '+zoom+' zl: '+zoomLevel);
        });


        /*google.maps.event.addListener(myMarker, 'dragend', function () {
    map.setCenter(this.getPosition()); // Set map center to marker position
    updatePosition(this.getPosition().lat(), this.getPosition().lng()); // update position display
});

google.maps.event.addListener(map, 'dragend', function () {
    myMarker.setPosition(this.getCenter()); // set marker position to map center
    updatePosition(this.getCenter().lat(), this.getCenter().lng()); // update position display
});

function updatePosition(lat, lng) {
    document.getElementById('dragStatus').innerHTML = '<p> Current Lat: ' + lat.toFixed(4) + ' Current Lng: ' + lng.toFixed(4) + '</p>';
}*/


var myMarker = new google.maps.Marker({
    position: center,
    draggable: true,
    map: map
});

google.maps.event.addListener(myMarker, 'dragend', function () {
    map.setCenter(this.getPosition()); // Set map center to marker position
    updatePosition(this.getPosition().lat(), this.getPosition().lng()); // update position display
    searchLocationsNear(centerUser);
});

google.maps.event.addListener(map, 'dragend', function () {
    myMarker.setPosition(this.getCenter()); // set marker position to map center
    updatePosition(this.getCenter().lat(), this.getCenter().lng()); // update position display
    searchLocationsNear(centerUser);
});


myMarker.setVisible(false); // maps API hide call       


   }

   function updatePosition(lat, lng) {
    centerUser = new google.maps.LatLng(lat,lng);
    document.getElementById('dragStatus').innerHTML = '<p> Current Lat: ' + lat.toFixed(4) + ' Current Lng: ' + lng.toFixed(4) + '</p>';
}


   function searchLocations() {
     var address = document.getElementById("addressInput").value;
     var geocoder = new google.maps.Geocoder();
     geocoder.geocode({address: address}, function(results, status) {
       if (status == google.maps.GeocoderStatus.OK) {
        console.log('results[0].geometry.location: '+results[0].geometry.location);
        centerUser = results[0].geometry.location;
        searchLocationsNear(results[0].geometry.location);
       } else {
         alert(address + ' not found');
       }
     });
   }

   function clearLocations() {
     infoWindow.close();
     for (var i = 0; i < markers.length; i++) {
       markers[i].setMap(null);
     }
     markers.length = 0;

     locationSelect.innerHTML = "";
     var option = document.createElement("option");
     option.value = "none";
     option.innerHTML = "See all results:";
     locationSelect.appendChild(option);
   }

function searchLocationsNear(center) {
  clearLocations();

  var radius = 5;
  var searchUrl = '/report/searchReport/' + center.lat() + '/' + center.lng() + '/' + radius;
  downloadUrl(searchUrl, function(data) {
    if(data == '') {
      alert('No se encontraron resultados!');
    } else {
       var xml = parseXml(data);
       var markerNodes = xml.documentElement.getElementsByTagName("marker");
       var bounds = new google.maps.LatLngBounds();
       for (var i = 0; i < markerNodes.length; i++) {
         var title = markerNodes[i].getAttribute("title");
         var address = markerNodes[i].getAttribute("address");
         var distance = parseFloat(markerNodes[i].getAttribute("distance"));
         var _id = parseFloat(markerNodes[i].getAttribute("_id"));
         var latlng = new google.maps.LatLng(
              parseFloat(markerNodes[i].getAttribute("lat")),
              parseFloat(markerNodes[i].getAttribute("lng")));

         var title = markerNodes[i].getAttribute("title");
         var description =

         createOption(title, distance, i);
         createMarker(latlng, title, address, _id);
         bounds.extend(latlng);
       }
       map.fitBounds(bounds);
       zoomLevel = map.getZoom();
       console.log(' zl: '+zoomLevel);
       locationSelect.style.visibility = "visible";
       locationSelect.onchange = function() {
         var markerNum = locationSelect.options[locationSelect.selectedIndex].value;
         google.maps.event.trigger(markers[markerNum], 'click');
       };
     }
      });
  
}

    function createMarker(latlng, title, address, _id) {
      var html = "<b>" + (title != '' ? title : '<i>Sin título</i>') + "</b> <br/>" + address + '<br><a class="map-detail-report" data-report="'+_id+'" data-toggle="modal" data-target="#myModal">Ver detalle</a><br>';
      var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        _id: _id
      });

      /*var marker = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: latlng,
            radius: 100,
      });*/
      
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
        //alert(marker._id);
      });
      markers.push(marker);
      //marker.addListener('click', toggleBounce);

    }

    /*function toggleBounce() {
      alert();
    }*/


    function createOption(name, distance, num) {
      var option = document.createElement("option");
      option.value = num;
      option.innerHTML = name + "(" + distance.toFixed(1) + ")";
      locationSelect.appendChild(option);
    }

    function downloadUrl(url, callback) {
      var request = window.ActiveXObject ?
          new ActiveXObject('Microsoft.XMLHTTP') :
          new XMLHttpRequest;

      request.onreadystatechange = function() {
        if (request.readyState == 4) {
          request.onreadystatechange = doNothing;
          callback(request.responseText, request.status);
        }
      };

      request.open('GET', url, true);
      request.send(null);
    }

    function parseXml(str) {
      if (window.ActiveXObject) {
        var doc = new ActiveXObject('Microsoft.XMLDOM');
        doc.loadXML(str);
        return doc;
      } else if (window.DOMParser) {
        return (new DOMParser).parseFromString(str, 'text/xml');
      }
    }

    function doNothing() {}

    function initMapDetail(latD, lngD) {
      var myLatLng = {lat: latD, lng: lngD};
      //alert('sssd');
      var mapDetailModal = new google.maps.Map(document.getElementById('mapDetail'), {
        zoom: 24,
        center: myLatLng
      });

      var markerDetailModal = new google.maps.Marker({
        position: myLatLng,
        map: mapDetailModal,
        title: 'Hello World!'
      });
    }