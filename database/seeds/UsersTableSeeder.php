<?php
/**
 * @author kwn
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'userGroups_id' => 1,
        	'name' => 'User',
        	'email' => 'user@server.com',
        	'password' => bcrypt('@secret@'),
        	'numberDocument' => '1',
        	'username' => 'user',
        	'lastName' => '',
            'birthdate' => '1991-09-20',
            'img' => '',
        	'role' => 1,
        	'status' => 2,
            'tokenEmail' => '',
        ]);
    }
}
