<?php
/**
 * @author kwn
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $state = 'types';

    protected $fillable = ['user_id','type_id','title', 'description', 'address', 'latitude', 'longitude', 'eventDate'];
}
