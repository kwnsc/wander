@extends('layouts.app')
@section('titleApp')
@lang('general.editUser')
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}"/>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&sensor=true"></script>
<script type="text/javascript" src="{{ asset('js/map-add-report.js') }}"></script>
@endsection

@section('content')
<div class="container bootstrap-iso">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('flash::message')
            <div class="panel panel-default">
                <div class="panel-heading">@lang('general.editUser')</div>
                <div class="panel-body">
                    <form id="add-report" class="form-horizontal" role="form" method="POST" action="{{ url('/user/update') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">@lang('general.name')</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastName') ? ' has-error' : '' }}">
                            <label for="lastName" class="col-md-4 control-label">@lang('general.lastName')</label>

                            <div class="col-md-6">
                                <input id="lastName" type="text" class="form-control" name="lastName" value="{{ $user->lastName }}" required>

                                @if ($errors->has('lastName'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lastName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('birthdate') ? ' has-error' : '' }}">
                            <label for="birthdate" class="col-md-4 control-label">@lang('general.birthdate')</label>

                            <div class="col-md-6">
                                <input class="form-control" id="birthdate" name="birthdate" value="{{ $user->birthdate }}" placeholder="DD/MM/YYYY" type="text" required>

                                @if ($errors->has('birthdate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('birthdate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>



                        <!--<div id="map_canvas_add_report" style="width: 100%; height: 300px;"></div>
                        <input type="hidden" id="lat" name="lat">
                        <input type="hidden" id="long" name="long">
                        <input type="hidden" id="adds" name="adds">
                        <br>-->

                        <div class="form-group">
                            <div class="col-md-6" align="right">
                                <button type="button" class="btn btn-default">
                                    @lang('general.cancel')
                                </button>
                            </div>
                            <div class="col-md-6" align="left">
                                <button type="submit" class="btn btn-primary">
                                    @lang('general.sendUpdate')
                                </button>
                            </div>
                        </div>

                        <div class="msg-error-report"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
