<?php
namespace App;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);
            $user = User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),

                    'userGroups_id' => 2,
                    'password' => '',
                    'numberDocument' => 0,
                    'username' => $providerUser->getEmail(),
                    'lastName' => '',
                    //'birthdate' => '',
                    'img' => '',
                    'role' => 0,
                    'status' => 1,
                    'tokenEmail' => '',
                ]);
            }
            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }
}