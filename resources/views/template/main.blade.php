<!DOCTYPE html>
<html>
<head>
	<title>@yield('title','Default')</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
	<script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	
</head>
<body>
	@include('template.menu')
	@yield('generalContend')
</body>
</html>