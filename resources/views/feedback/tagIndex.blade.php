            <form id="add-feedback" class="form-horizontal" role="form" method="POST" action="{{ url('/feedback/store') }}">
            {{ csrf_field() }}
            @if($isModal)
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                @endif
                    

                        @if(!$isSession)
                        <div class="form-group{{ $errors->has('names') ? ' has-error' : '' }}">
                            <label for="names" class="col-md-4 control-label">@lang('general.names')</label>

                            <div class="col-md-6">
                                <input id="names" type="text" class="form-control" name="names" value="{{ old('names') }}" required>

                                @if ($errors->has('names'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('names') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">@lang('general.emailAddress')</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">@lang('general.category')</label>

                            <div class="col-md-6">
                                <select id="type" class="form-control" name="type" value="{{ old('type') }}">
                                    <option value="1">Error</option>
                                    <option value="2">Etc</option>
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">@lang('general.description')</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control" name="description" value="{{ old('description') }}" required></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(!$isModal)
                        <div class="form-group">
                            <div class="col-md-6" align="right">
                                <button type="button" class="btn btn-default">
                                    @lang('general.cancel')
                                </button>
                            </div>
                            <div class="col-md-6" align="left">
                                <button type="submit" class="btn btn-primary">
                                    @lang('general.sendAdd')
                                </button>
                            </div>
                        </div>

                        </form>
                        @endif
                        <div class="msg-error-feedback"></div>
                    
                @if($isModal)
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">@lang('general.sendAdd')</button>
                </div>
            </div>
            </div>
            </form>
            @endif