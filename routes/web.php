<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*Route::get('/login', function () {
    return view('security.login')->with('isModal', false);
});
Route::get('/register', function () {
    return view('security.register')->with('isModal', false);
});
*/
Route::post('/getTagLogin', function () {
    return view('auth.tagLogin')->with('isModal', true);
});
Route::post('/getTagRegister', function () {
    return view('auth.tagRegister')->with('isModal', true);
});

Route::get('/report', 'ReportController@index');
Route::get('/report/create', 'ReportController@create');
Route::post('/report/store', 'ReportController@store');
Route::get('/report/searchReport/{lat}/{long}/{radio}', 'ReportController@searchInMap');
Route::get('/report/listReport/{title}/{address}/{dateEvent}', 'ReportController@listReport');
Route::post('/report/getDetailMapReport', 'ReportController@getDetailMapReport');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/user/confirmation/{token}', 'UserController@confirmation');
Route::get('/user/edit', 'UserController@edit');
Route::post('/user/update', 'UserController@update');

Route::get('/redirect', 'SocialAuthController@redirect');
Route::get('/callback', 'SocialAuthController@callback');

Route::get('/feedback', 'FeedbackController@index');
Route::post('/feedback/getTagIndex', 'FeedbackController@getTagIndex');
Route::post('/feedback/store', 'FeedbackController@store');