@extends('layouts.app')
@section('titleApp')
@lang('general.signup')
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('general.signup')</div>
                <div class="panel-body">
                    @include('auth.tagRegister')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
