/**
 * @author kwn
 */
var lat = null;
var lng = null;
var map = null;
var geocoder = null;
var marker = null;
         
jQuery(document).ready(function() {
  console.log('ready');
  lat = jQuery('#lat').val();
  lng = jQuery('#long').val();
  jQuery('#search-address').click(function() {
    codeAddress();
    return false;
  });
  initializeMapAdd();
  codeAddress();
  jQuery( '#add-report' ).on( 'submit', function( event ) {
    validateAddReport(event);
  });
});
     
function initializeMapAdd() {
  geocoder = new google.maps.Geocoder();
  if(lat !='' && lng != '') {
    var latLng = new google.maps.LatLng(lat,lng);
  } else {
    var latLng = new google.maps.LatLng(-5.1782884,-80.65488820000002);
  }

  var myOptions = {
    center: latLng,//centro del mapa
    zoom: 15,//zoom del mapa
    mapTypeId: google.maps.MapTypeId.ROADMAP //tipo de mapa, carretera, híbrido,etc
  };
  map = new google.maps.Map(document.getElementById("map_canvas_add_report"), myOptions);
         
  marker = new google.maps.Marker({
    map: map,//el mapa creado en el paso anterior
    position: latLng,//objeto con latitud y longitud
    draggable: true //que el marcador se pueda arrastrar
  });

  updatePosition(latLng);
}
     
    //funcion que traduce la direccion en coordenadas
    function codeAddress() {
         
        //obtengo la direccion del formulario
        var address = document.getElementById("address").value;
        //hago la llamada al geodecoder
        geocoder.geocode( { 'address': address}, function(results, status) {
         
        //si el estado de la llamado es OK
        if (status == google.maps.GeocoderStatus.OK) {
            //centro el mapa en las coordenadas obtenidas
            map.setCenter(results[0].geometry.location);
            //coloco el marcador en dichas coordenadas
            marker.setPosition(results[0].geometry.location);
            //actualizo el formulario      
            updatePosition(results[0].geometry.location);

            updateMarkerAddress(results[0].formatted_address);
             
            //Añado un listener para cuando el markador se termine de arrastrar
            //actualize el formulario con las nuevas coordenadas
            google.maps.event.addListener(marker, 'dragend', function(){
                updatePosition(marker.getPosition());
                codeLatLng(marker.getPosition())
                //updateMarkerAddress(marker.formatted_address);
                console.log('puto drag');
            });
      } else {
          //si no es OK devuelvo error
          alert("No podemos encontrar la direcci&oacute;n, error: " + status);
      }
    });
  }

  function codeLatLng(latLng) {
	  var latlng = new google.maps.LatLng(latLng.lat(), latLng.lng());
	  geocoder.geocode({
	    'latLng': latlng
	  }, function (results, status) {
	    if (status === google.maps.GeocoderStatus.OK) {
	      if (results[0]) {
	        console.log(results[0].formatted_address+' - '+results[1].formatted_address);
	        updateMarkerAddress(results[0].formatted_address);
	      } else {
	        alert('No results found');
	      }
	    } else {
	      alert('Geocoder failed due to: ' + status);
	    }
	  });
	}
   
  //funcion que simplemente actualiza los campos del formulario
  function updatePosition(latLng)
  {
       
       jQuery('#lat').val(latLng.lat());
       jQuery('#long').val(latLng.lng());
   
  }

  function updateMarkerAddress(str) {
  	jQuery('#adds').val(str);
  }

  function validateAddReport(event) {
    if(lat == '' || long == '') {
      event.preventDefault();
      jQuery('.msg-error-report').html('<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-info-sign"></span> Selecciona al menos una dirección referencial</div>');
    }
  }