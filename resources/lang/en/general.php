<?php
/**
 * @author kwn
 */

return array(
	'welcome' => 'Welcome to',
	'home' => 'Home',
	'search' => 'Search',
	'login' => 'Login',
	'signup' => 'Register',
	'editUser' => 'Edit user',
	'logout' => 'Logout',

	'registerDenounce' => 'Register Denounce',
	'numberDocument' => 'Number document',
	'typeDenounce' => 'Type of denounce',
	'titleDenounce' => 'Title of denounce',
	'descriptionDenounce' => 'Description',
	'eventDate' => 'Event date',
	'address' => 'Address',

	'emailAddress' => 'E-mail address',
	'password' => 'Password',
	'rememberMe' => 'Remember me',
	'forgotYourPassword' => 'Forgot your password?',
	'loginWithFacebook' => 'Login with Facebook',
	'signupWithFacebook' => 'Signup with Facebook',

	'name' => 'Names',
	'lastName' => 'Surnames',
	'confirmPassword' => 'Confirm password',
	'hi' => 'Hi',
	'welcomeConfirmation' => 'Welcome to Wander.pe',
	'confirmAccount' => 'Confirm account',

	'verifiedAccount' => 'Verified account',
	'accountConfirmed' => 'Account Confirmed!',
	'goToHome' => 'Go to home',
	'failedToConfirmAccount' => 'Failed to confirm account',

	'cancel' => 'Cancel',
	'sendAdd' => 'Register',
	'sendUpdate' => 'Update',

	'birthdate' => 'Birthdate',

	'description' => 'Description',
	'names' => 'Names',
	'category' => 'Category',

	'hasToBeConfirmed' => 'Has to be confirmed',
	'findPlace' => 'Find place',
);