<?php
/**
 * @author kwn
 */
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
        	[
        		'name' => 'ABIGEATO',
        		'description' => 'Hurto de ganado',
        	],
        	[
        		'name' => 'DAÑOS',
        		'description' => '',
        	],
        	[
        		'name' => 'DELITOS INFORMATICOS',
        		'description' => '',
        	],
        	[
        		'name' => 'ESTAFA Y OTRAS DEFRAUDACIONES',
        		'description' => '',
        	],
        	[
        		'name' => 'HURTO',
        		'description' => '',
        	],
        	[
        		'name' => 'RECEPTACION',
        		'description' => '',
        	],
        	[
        		'name' => 'ROBO',
        		'description' => '',
        	],
        	[
        		'name' => 'USURPACION',
        		'description' => '',
        	],
        ]);
    }
}
