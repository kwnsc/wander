<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class SecurityController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }

    public function login()
    {
        //return view('user.profile', ['user' => User::findOrFail($id)]);
        return view('security.login')->with('isModal', false);
    }
}