<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@lang('general.welcome') Wander.pe</title>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&libraries=places" type="text/javascript"></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&libraries=places&callback=initMap"
        async defer></script>-->
        <script type="text/javascript" src="{{ asset('js/search-map-report.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/search-map-police-station.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/application.js') }}"></script>
    </head>
    <body>
        @include('layouts.menu')
        <div class="presentation">
            <div class="container"></div>
        </div>
        @include('flash::message')
        <div >
        <div class="container search-index tabbable">
            <div class="search-reports">
            <!--<ul class="nav nav-tabs">
                <li class="active"><a href="#pane1" data-toggle="tab">Denuncias</a></li>
                <li><a href="#pane2" data-toggle="tab">Comisarias</a></li>
            </ul>-->
            <!--<div class="tab-content">
                <div id="pane1" class="search-reports tab-pane active">-->
                        <div class="input-group">
                            <!--put type="text" class="form-control" placeholder="Buscar lugar" id="text-search-report-map">-->
                            <input type="text" class="form-control" placeholder="@lang('general.findPlace')" id="addressInput">

                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick="searchLocations()"><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                            <!--<div class="container-map-search"></div>-->
                        </div>
                        <div><select id="locationSelect" style="width:100%;visibility:hidden"></select></div>
                        <div id="map" style="width: 100%; height: 600px"></div>

                        <div id="dragStatus"></div>
                </div>
                <!--</div>
                <div id="pane2" class="search-comisaries tab-pane">-->
                <div class="search-police-station">
                    <br>
                    <div class="input-group">
                            <input type="text" class="form-control" placeholder="@lang('general.findPS')" id="autocomplete">

                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" onclick=""><span class="glyphicon glyphicon-search"></span></button>
                            </span>
                            <!--<div class="container-map-search"></div>-->
                    </div>

                    <div id="mapps"></div>

                    <div id="listing" class="listing-police-station">
                      <table id="resultsTable">
                        <tbody id="results"></tbody>
                      </table>
                    </div>

                    <div style="display: none">
                      <div id="info-content">
                        <table>
                          <tr id="iw-url-row" class="iw_table_row">
                            <td id="iw-icon" class="iw_table_icon"></td>
                            <td id="iw-url"></td>
                          </tr>
                          <tr id="iw-address-row" class="iw_table_row">
                            <td class="iw_attribute_name">Address:</td>
                            <td id="iw-address"></td>
                          </tr>
                          <tr id="iw-phone-row" class="iw_table_row">
                            <td class="iw_attribute_name">Telephone:</td>
                            <td id="iw-phone"></td>
                          </tr>
                          <tr id="iw-rating-row" class="iw_table_row">
                            <td class="iw_attribute_name">Rating:</td>
                            <td id="iw-rating"></td>
                          </tr>
                          <tr id="iw-website-row" class="iw_table_row">
                            <td class="iw_attribute_name">Website:</td>
                            <td id="iw-website"></td>
                          </tr>
                        </table>
                      </div>
                    </div>
                </div>
                <!--</div>
            </div>--><!-- /.tabbable -->
           
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"></div>
    </body>
</html>