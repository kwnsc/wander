@extends('layouts.app')
@section('titleApp')
@lang('general.registerDenounce')
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}"/>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDWjCwee_hFUPYuqHq4rBtE0rMESROJcWc&sensor=true"></script>
<script type="text/javascript" src="{{ asset('js/map-add-report.js') }}"></script>
@endsection

@section('content')
<div class="container bootstrap-iso">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('general.registerDenounce')</div>
                <div class="panel-body">
                    <form id="add-report" class="form-horizontal" role="form" method="POST" action="{{ url('/report/store') }}">
                        {{ csrf_field() }}

                        @if($user->numberDocument == 0)
                        <div class="form-group{{ $errors->has('numberDocument') ? ' has-error' : '' }}">
                            <label for="numberDocument" class="col-md-4 control-label">@lang('general.numberDocument')</label>

                            <div class="col-md-6">
                                <input id="numberDocument" type="text" class="form-control" name="numberDocument" value="{{ old('numberDocument') }}" required>

                                @if ($errors->has('numberDocument'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('numberDocument') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @endif

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">@lang('general.typeDenounce')</label>

                            <div class="col-md-6">
                                <select id="type" class="form-control" name="type" value="{{ old('type') }}">
                                    @foreach($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">@lang('general.titleDenounce')</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">@lang('general.descriptionDenounce')</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control" name="description" value="{{ old('description') }}" required></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('eventDate') ? ' has-error' : '' }}">
                            <label for="eventDate" class="col-md-4 control-label">@lang('general.eventDate')</label>

                            <div class="col-md-6">
                                <input class="form-control" id="eventDate" name="eventDate" value="{{ old('eventDate') }}" placeholder="MM/DD/YYYY HH:MM:SS" type="text" required>

                                @if ($errors->has('eventDate'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('eventDate') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">@lang('general.address')</label>

                            <div class="col-md-6">

                                <div class="input-group">
                                    <input id="address" type="text" class="form-control" name="address" value="{{ old('address') == '' ? 'Piura, Peru' : old('address') }}" required>
                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" id="search-address"><span class="glyphicon glyphicon-search"></span></button>
                                    </span>
                                </div><!-- /input-group -->
                                
                            </div>
                        </div>

                        <div id="map_canvas_add_report" style="width: 100%; height: 300px;"></div>
                        <input type="hidden" id="lat" name="lat">
                        <input type="hidden" id="long" name="long">
                        <input type="hidden" id="adds" name="adds">
                        <br>

                        <div class="form-group">
                            <div class="col-md-6" align="right">
                                <button type="button" class="btn btn-default">
                                    @lang('general.cancel')
                                </button>
                            </div>
                            <div class="col-md-6" align="left">
                                <button type="submit" class="btn btn-primary">
                                    @lang('general.sendAdd')
                                </button>
                            </div>
                        </div>

                        <div class="msg-error-report"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
