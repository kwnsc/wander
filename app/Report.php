<?php
/**
 * @author kwn
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $state = 'reports';

    protected $fillable = ['user_id','type_id','title', 'description', 'address', 'latitude', 'longitude', 'eventDate'];

    public function type()
    {
    	return $this->belongsTo('App\Type');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
